﻿using System.Web;
using System.Web.Mvc;

namespace X.Tiss.Rest.MonitoracaoTransporte
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}

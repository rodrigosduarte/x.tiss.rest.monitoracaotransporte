﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using X.Tiss.Rest.MonitoracaoTransporte.Models;

namespace X.Tiss.Rest.MonitoracaoTransporte.Controllers
{
    public class MaquinasController : Controller
    {
        public MaquinaProcessReturn Consultar([FromBody] X.Tiss.Content.Models.Maquinas dados)
        {
            MaquinasActions actions = new MaquinasActions();
            var retorno = actions.Consultar(dados);

            return retorno;
        }

        public MaquinaProcessReturn Incluir([FromBody] X.Tiss.Content.Models.Maquinas dados)
        {
            MaquinasActions actions = new MaquinasActions();
            var retorno = actions.Incluir(dados);

            return retorno;
        }

        public MaquinaProcessReturn Alterar([FromBody] X.Tiss.Content.Models.Maquinas dados)
        {
            MaquinasActions actions = new MaquinasActions();
            var retorno = actions.Alterar(dados);

            return retorno;
        }
    }
}
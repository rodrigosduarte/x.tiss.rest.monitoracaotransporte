﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using X.Tiss.Rest.MonitoracaoTransporte.Models;

namespace X.Tiss.Rest.MonitoracaoTransporte.Controllers
{
    public class Maquinas_NotificacoesController : Controller
    {
        public Maquinas_NotificacoesProcessReturn Incluir([FromBody] X.Tiss.Content.Models.Maquinas_Notificacoes dados)
        {
            Maquinas_NotificacoesActions actions = new Maquinas_NotificacoesActions();

            dados.NOM_IP = System.Web.HttpContext.Current.Request.UserHostAddress;
            var retorno = actions.Incluir(dados);

            return retorno;
        }
    }
}
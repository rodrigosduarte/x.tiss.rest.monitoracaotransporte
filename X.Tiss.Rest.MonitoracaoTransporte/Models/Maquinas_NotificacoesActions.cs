﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace X.Tiss.Rest.MonitoracaoTransporte.Models
{
    public class Maquinas_NotificacoesActions
    {
        string conn = System.Configuration.ConfigurationManager.ConnectionStrings["str_conn"].ToString();

        public Maquinas_NotificacoesProcessReturn Incluir(X.Tiss.Content.Models.Maquinas_Notificacoes dados)
        {
            Maquinas_NotificacoesProcessReturn processado = new Maquinas_NotificacoesProcessReturn();
            processado.Erros = new List<string>();
            
            if (dados.COD_MAQUINA != Guid.Empty)
                processado.Erros.Add("O hash da maquina não foi informado.");

            if (string.IsNullOrEmpty(dados.NOM_IP))
                processado.Erros.Add("O IP não foi informado.");

            if (processado.Erros.Count == 0)
            {
                X.Tiss.Content.Models.Resultado cadastro = dados.Inserir(conn);

                processado.Processado = cadastro.COD_RESULTADO > 0;

                if (cadastro.COD_RESULTADO > 0)
                {
                    processado.Ticket = dados;
                }
                else
                {
                    processado.Erros.Add(cadastro.DSC_RESULTADO);
                }
            }

            return processado;
        }
    }
}
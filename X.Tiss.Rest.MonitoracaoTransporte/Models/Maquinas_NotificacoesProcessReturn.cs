﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace X.Tiss.Rest.MonitoracaoTransporte.Models
{
    public class Maquinas_NotificacoesProcessReturn
    {
        public bool Processado { get; set; }
        public IList<string> Erros { get; set; }
        public X.Tiss.Content.Models.Maquinas_Notificacoes Ticket { get; set; }
    }
}
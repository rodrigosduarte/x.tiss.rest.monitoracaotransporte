﻿using System.Collections.Generic;

namespace X.Tiss.Rest.MonitoracaoTransporte.Models
{
    public class PrestadorProcessReturn
    {
        public bool Processado { get; set; }
        public IList<string> Erros { get; set; }
        public X.Tiss.Content.Models.Prestadores Prestador { get; set; }
    }
}
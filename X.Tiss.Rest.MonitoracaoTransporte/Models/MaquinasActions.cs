﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace X.Tiss.Rest.MonitoracaoTransporte.Models
{
    public class MaquinasActions
    {
        string conn = System.Configuration.ConfigurationManager.ConnectionStrings["str_conn"].ToString();

        public MaquinaProcessReturn Consultar(X.Tiss.Content.Models.Maquinas dados)
        {
            MaquinaProcessReturn processado = new MaquinaProcessReturn();
            processado.Erros = new List<string>();

            if (dados.COD_MAQUINA != Guid.Empty && dados.HASH_PRESTADOR != Guid.Empty)
            {
                List<X.Tiss.Content.Models.Maquinas> cadastro = dados.Listar(conn);

                if (cadastro != null && cadastro.Count > 0)
                {
                    processado.Processado = true;
                    processado.Maquina = cadastro[0];
                }
                else
                {
                    processado.Processado = false;
                    processado.Erros = new List<string>();
                    processado.Erros.Add("Falha na inclusão da máquina, por favor, tente novamente ou entre em contato com a equipe de suporte.");
                }
            }
            else
            {
                processado.Processado = false;
                processado.Erros = new List<string>();
                processado.Erros.Add("Informações inválidas, por favor, tente novamente ou entre em contato com a equipe de suporte.");
            }

            return processado;
        }
        
        public MaquinaProcessReturn Incluir(X.Tiss.Content.Models.Maquinas dados)
        {
            var processado = new MaquinaProcessReturn();
            processado.Erros = new List<string>();

            if (dados.HASH_PRESTADOR != Guid.Empty)
                processado.Erros.Add("O hash do prestador não foi informado.");

            if (string.IsNullOrEmpty(dados.NOM_DOMINIO))
                processado.Erros.Add("O dominio não foi informado.");

            if (string.IsNullOrEmpty(dados.NOM_MAQUINA))
                processado.Erros.Add("O nome da maquina não foi informado.");

            if (string.IsNullOrEmpty(dados.NOM_IP))
                processado.Erros.Add("O IP não foi informado.");

            if (string.IsNullOrEmpty(dados.NOM_USUARIO))
                processado.Erros.Add("O nome do usuário não foi informado.");
            
            if (processado.Erros.Count == 0)
            {
                X.Tiss.Content.Models.Resultado cadastro = dados.Inserir(conn);
                                
                processado.Processado = cadastro.COD_RESULTADO > 0;

                if (cadastro.COD_RESULTADO > 0)
                {
                    processado.Maquina = dados;
                }
                else
                {
                    processado.Erros.Add(cadastro.DSC_RESULTADO);                   
                }
            }
            
            return processado;
        }

        public MaquinaProcessReturn Alterar(X.Tiss.Content.Models.Maquinas dados)
        {
            var processado = new MaquinaProcessReturn();
            processado.Erros = new List<string>();

            if (dados.COD_MAQUINA != Guid.Empty)
                processado.Erros.Add("O hash da maquina não foi informado.");

            if (dados.HASH_PRESTADOR != Guid.Empty)
                processado.Erros.Add("O hash do prestador não foi informado.");

            if (string.IsNullOrEmpty(dados.NOM_DOMINIO))
                processado.Erros.Add("O dominio não foi informado.");

            if (string.IsNullOrEmpty(dados.NOM_MAQUINA))
                processado.Erros.Add("O nome da maquina não foi informado.");

            if (string.IsNullOrEmpty(dados.NOM_IP))
                processado.Erros.Add("O IP não foi informado.");

            if (string.IsNullOrEmpty(dados.NOM_USUARIO))
                processado.Erros.Add("O nome do usuário não foi informado.");

            if (processado.Erros.Count == 0)
            {
                X.Tiss.Content.Models.Resultado cadastro = dados.Alterar(conn);

                processado.Processado = cadastro.COD_RESULTADO > 0;

                if (cadastro.COD_RESULTADO > 0)
                {
                    processado.Maquina = dados;
                }
                else
                {
                    processado.Erros.Add(cadastro.DSC_RESULTADO);
                }
            }

            return processado;
        }
    }
}
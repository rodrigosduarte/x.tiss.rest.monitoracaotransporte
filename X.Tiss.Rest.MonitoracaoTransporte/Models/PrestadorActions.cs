﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace X.Tiss.Rest.MonitoracaoTransporte.Models
{
    public class PrestadorActions
    {
        string conn = System.Configuration.ConfigurationManager.ConnectionStrings["str_conn"].ToString();

        public PrestadorProcessReturn Consultar(X.Tiss.Content.Models.Prestadores dados)
        {
            var processado = new PrestadorProcessReturn();

            if (dados.HASH != Guid.Empty)
            {
                X.Tiss.Content.Models.Prestadores cadastro = new X.Tiss.Content.Models.Prestadores().Carregar(dados.HASH, conn);

                    processado.Prestador = cadastro;
                processado.Processado = cadastro.RESULTADO == null;

                if (cadastro.RESULTADO != null && cadastro.RESULTADO.Count > 0)
                {
                    processado.Erros = new List<string>();
                    processado.Erros.Add(cadastro.RESULTADO[0].DSC_RESULTADO);
                }
            }
            else
            {
                processado.Processado = false;
                processado.Erros = new List<string>();
                processado.Erros.Add("Falha no processo de consulta do prestador, por favor, tente novamente ou entre em contato com a equipe de suporte.");
            }

            return processado;
        }

    }
}
﻿using System.Collections.Generic;

namespace X.Tiss.Rest.MonitoracaoTransporte.Models
{
    public class MaquinaProcessReturn
    {
        public bool Processado { get; set; }
        public IList<string> Erros { get; set; }
        public X.Tiss.Content.Models.Maquinas Maquina { get; set; }
    }
}